# Horror on Holiday Rails

Play in-browser:
https://consistency.itch.io/horror-on-holiday-rails

Playthrough:
https://www.youtube.com/watch?v=Av1gnMez2Lw

This game was made in 7 days for the Scream Season game jam. Your goal is to collect the presents from the front of the train and bring them to the back without getting caught by the horror patrolling the train cars. 

NOTE: Due to browser updates, the Armory Engine can no longer lock your mouse in windowed mode or on anything besides Chrome. Firefox will not lock your mouse and you will be unable to turn unless you use the arrow keys which are assigned to tank controls. Internet Explorer/Edge is not supported by the Armory engine because that browser is trash. If you're going to play in browser, you must play it on Chrome in fullscreen for mouse input, use the tank controls/arrow keys, otherwise download the game. If you're experiencing performance or loading issues, we also recommend the downloadable version. If this game is run locally on a hard drive via index.html, Firefox must be used since Chrome blocks local scripts.

We hope you enjoy this short game!

